
import Constants from './Constants.js';

// State
let companies = [];
let displayMode = Constants.DISPLAY_MODE_LIST;

// Access methods
function getCompanies() {
    return companies;
}

function setCompanies(companyList) {
    companies = companyList;
}

function clearCompanies() {
    companies = [];
}

function getCompany(id) {
    return companies.find(c => c.id === id);
}

function updateCompany(company) {
    let index = companies.findIndex(c => c.id === company.id);
    if (index >= 0) {
        companies[index] = company;
    }
}

function getDisplayMode() {
    return displayMode;
}

function toggleDisplayMode() {
    if (displayMode === Constants.DISPLAY_MODE_LIST) {
        displayMode = Constants.DISPLAY_MODE_CHART;
    } else {
        displayMode = Constants.DISPLAY_MODE_LIST;
    }
}

export default {
    getCompanies,
    setCompanies,
    clearCompanies,
    getCompany,
    updateCompany,
    getDisplayMode,
    toggleDisplayMode
};