import App from '../App.js';
import AppState from '../AppState.js';
import Constants from '../Constants.js';

const template = document.createElement('template');
template.innerHTML = /*html*/`
<div class="item-fluid">
    <div class="item-100-center">
        <h1>Listed Companies</h1>
    </div>
</div>
<div class="item-fluid">
    <div class="item-100-center">
        <button class="button-primary">Show chart</button>
    </div>
</div>
`;

class CompanyTopSection extends HTMLElement {

    connectedCallback() {
        this.render();
    }

    render() {
        this.classList.add("items");
        let contentNode = document.importNode(template.content, true);
        this.innerHTML = '';
        this.appendChild(contentNode);
        this.querySelector('button').addEventListener('click', this.toggle);
        this.querySelector('button').innerText =
            AppState.getDisplayMode() === Constants.DISPLAY_MODE_LIST ? 'Show chart' : 'Show list';
    }

    toggle = () => {
        AppState.toggleDisplayMode();
        this.render();        
        App.load(false);
    }
}

window.customElements.define('company-top-section', CompanyTopSection);