export default {
    default: {
        dimensionClass: 'popup-600-800',
        displayCloseButton: true,
        coverBackground: 'darkgrey',
        coverOpacity: '95%',
        coverCloseOnClick: true
    },
    blank_300_300: {
        dimensionClass: 'popup-300-300',
        displayCloseButton: false,
        coverBackground: 'white',
        coverOpacity: '100%',
        coverCloseOnClick: false
    },
    blank_400_400: {
        dimensionClass: 'popup-400-400',
        displayCloseButton: false,
        coverBackground: 'white',
        coverOpacity: '100%',
        coverCloseOnClick: false
    }
};
