import AppState from '../AppState.js';
import Constants from '../Constants.js';

const template = document.createElement('template');
template.innerHTML = /*html*/`
<section data-key="companies" class="items"></section>
<div class="items">
    <div class="item-fluid">
        <div class="item-100-center">
            <button data-key="addButton" class="button-primary">Lisa ettevõte</button>
        </div>
    </div>
</div>
<company-add data-key="addCompanyPopup"></company-add>
`;

class CompanyList extends HTMLElement {

    render() {
        this.innerHTML = '';
        if (AppState.getDisplayMode() === Constants.DISPLAY_MODE_LIST) {
            let contentNode = document.importNode(template.content, true);
            this.appendChild(contentNode);
            this.querySelector('[data-key=companies]').innerHTML =
            /*html*/`${AppState.getCompanies().map(c => /*html*/`<company-item company-id="${c.id}" class="item"></company-item>`).join('')}`;
            this.querySelector('[data-key=addButton]').addEventListener('click', this.openPopup);
        }
    }

    openPopup = () => {
        this.querySelector('[data-key=addCompanyPopup]').openPopup();
    }
}

window.customElements.define('company-list', CompanyList);