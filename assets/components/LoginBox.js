import App from '../App.js';
import Auth from '../Auth.js';
import FetchApi from '../FetchApi.js';
import Util from '../Util.js';

const template = document.createElement('template');
template.innerHTML = /*html*/`
<popup-window config-name="blank_300_300">
    <template>
        <div id="loginFormContainer">    
            <div>
                <h2>Authentication</h2>
                <error-box></error-box>
                <label>Username:</label>
                <input type="text" name="username">
                <label>Password:</label>
                <input type="password" name="password">
                <button name="login">Login</button>
            </div>
        </div>
    </template>
</popup-window>
`;

class LoginBox extends HTMLElement {

    connectedCallback() {
        this.render();
    }

    render() {
        let contentNode = document.importNode(template.content, true);
        this.appendChild(contentNode);
        this.querySelector('[name=login]').addEventListener('click', this.login);
    }

    openPopup = () => {
        let popup = this.querySelector('popup-window');
        popup.openPopup();
    }

    closePopup = () => {
        let popup = this.querySelector('popup-window');
        popup.closePopup();
    }

    login = async () => {
        let errors = [];
        try {
            let username = this.querySelector('[name=username]').value;
            let password = this.querySelector('[name=password]').value;

            let credentials = { username, password };

            if (Util.isEmpty(username)) {
                errors.push('Username missing.');
            } else if (Util.isEmpty(password)) {
                errors.push('Password missing.');
            }

            if (errors.length === 0) {
                const session = await FetchApi.login(credentials);
                Auth.storeAuthentication(session);
                this.closePopup();
                App.load();
            }
        } catch (e) {
            this.closePopup();
            Util.handleError(e, App.openLoginBox);
        } finally {
            this.querySelector('error-box').errors = errors;
        }
    }
}

window.customElements.define('login-box', LoginBox);