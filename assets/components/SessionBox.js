import App from '../App.js';
import Auth from '../Auth.js';
import AppState from '../AppState.js';

class SessionBox extends HTMLElement {
    render() {
        this.innerHTML = /*html*/`<strong>${Auth.getUsername()}</strong> | <a href="#">logout</a>`;
        this.querySelector('a').addEventListener('click', this.logout);
    }

    logout = () => {
        Auth.clearAuthentication();
        AppState.clearCompanies();
        App.load();
    }
}

window.customElements.define('session-box', SessionBox);