class ErrorBox extends HTMLElement {

    errorList = [];

    set errors(errors) {
        this.errorList = errors;
        this.render();
    }

    render() {
        let errorsHtml = /*html*/`<div><strong>Please fix the following issues:</strong></div>`;
        errorsHtml += this.errorList.map(error => /*html*/`<div>${error}</div>`).join("");
        this.innerHTML = errorsHtml;
        this.style.display = this.errorList.length > 0 ? 'block' : 'none';
    }
}

window.customElements.define('error-box', ErrorBox);