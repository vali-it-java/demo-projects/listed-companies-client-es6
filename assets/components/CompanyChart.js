import AppState from '../AppState.js';
import Constants from '../Constants.js';

const template = document.createElement('template');
template.innerHTML = /*html*/`
<div class="items">
    <div class="item-fluid">
        <canvas data-key="chartCanvas" style="height:40vh; width:80%"></canvas>
    </div>
</div>
`;

class CompanyChart extends HTMLElement {
    render() {
        this.innerHTML = '';
        if (AppState.getDisplayMode() === Constants.DISPLAY_MODE_CHART) {
            let contentNode = document.importNode(template.content, true);
            this.appendChild(contentNode);

            let ctx = this.querySelector('[data-key=chartCanvas]').getContext('2d');
            let chartData = composeChartData(AppState.getCompanies());
            new Chart(ctx, {
                type: 'pie',
                data: chartData,
                options: {}
            });
        }
    }
}

window.customElements.define('company-chart', CompanyChart);

function generateRandomColor() {
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);
    return `rgb(${r},${g},${b})`;
}

function composeChartDataset(items) {
    if (items != null && items.length > 0) {
        let data = items.map(item => Math.round(item.marketCapitalization));
        let backgroundColors = items.map(generateRandomColor);
        return [{
            data: data,
            backgroundColor: backgroundColors,
        }];
    }
    return [];
}

function composeChartLabels(items) {
    return items != null && items.length > 0 ? items.map(item => item.name) : [];
}

function composeChartData(companies) {
    return {
        datasets: composeChartDataset(companies),
        labels: composeChartLabels(companies)
    };
}
