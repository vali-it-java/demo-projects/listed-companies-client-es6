import App from '../App.js';
import AppState from '../AppState.js';
import Util from '../Util.js';
import FetchApi from '../FetchApi.js';

class CompanyItem extends HTMLElement {

    static get observedAttributes() {
        return ['company-id'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case 'company-id':
                this.companyId = parseInt(newValue);
                break;
        }
    }

    connectedCallback() {
        this.render();
    }

    render() {
        if (this.companyId > 0) {
            let company = AppState.getCompany(this.companyId);
            this.innerHTML = /*html*/`
                <h2 class="item-100">${company.name}</h2>
                <div class="item-100"><img class="item-logo" src="${company.logo}"></div>
                <div class="item-50-bold">Established</div>
                <div class="item-50">${company.established}</div>
                <div class="item-50-bold">Employees</div>
                <div class="item-50">${Util.formatNumber(company.employees)}</div>
                <div class="item-50-bold">Revenue</div>
                <div class="item-50">${Util.formatNumber(company.revenue)} &euro;</div>
                <div class="item-50-bold">Net income</div>
                <div class="item-50">${Util.formatNumber(company.netIncome)} &euro;</div>
                <div class="item-50-bold">Market capitalization</div>
                <div class="item-50">${Util.formatNumber(parseInt(company.marketCapitalization))} &euro;</div>
                <div class="item-50-bold">Dividend yield</div>
                <div class="item-50">${Util.formatNumber(company.dividendYield * 100)} %</div>
                <div class="item-100-center">
                    <button data-key="modifyButton">Modify</button> 
                    <button data-key="deleteButton" class="button-red">Delete</button>
                </div>
                <company-edit company-id="${company.id}"></company-edit>
            `;

            this.querySelector('[data-key=modifyButton]').addEventListener('click', this.openPopup);
            this.querySelector('[data-key=deleteButton]').addEventListener('click', this.delete);
        }
    }

    delete = async () => {
        try {
            if (confirm('Ara you sure you want to delete this company?')) {
                await FetchApi.deleteCompany(this.companyId);
                App.load();
            }
        } catch (e) {
            Util.handleError(e, App.openLoginBox);
        }
    }

    openPopup = () => {
        let popup = this.querySelector('company-edit');
        popup.openPopup();
    }
}

window.customElements.define('company-item', CompanyItem);