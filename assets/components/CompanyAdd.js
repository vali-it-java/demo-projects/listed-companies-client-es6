import CompanyEdit from './CompanyEdit.js';
import App from '../App.js';

class CompanyAdd extends CompanyEdit {
    
    bind() {
        // Adding a new company, nothing to bind.
    }

    companySavedCallback() {
        App.load();
    }
}

window.customElements.define('company-add', CompanyAdd);