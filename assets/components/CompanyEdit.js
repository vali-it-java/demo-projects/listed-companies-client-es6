import App from '../App.js';
import AppState from '../AppState.js';
import FetchApi from '../FetchApi.js';
import Util from '../Util.js';
import CompanyValidator from '../CompanyValidator.js';

const template = document.createElement('template');
template.innerHTML = /*html*/`
<popup-window config-name="default">
    <template>
        <div data-key="companyEditFormContainer">
            <h2>Modify Company</h2>
            <error-box></error-box>
            <div>
                <label>Name</label>
                <input type="text" name="name">
                <div><img data-key="logoImg"></div>
                <label>Logo</label>
                <input type="text" name="logo">
                <input type="file" name="file" title="Upload logo file" />
                <div><button name="upload" class="btn btn-dark">Upload</button></div>
                <label>Established</label>
                <input type="date" name="established">
                <label>Employees</label>
                <input type="number" min="0" max="1000000" step="1" name="employees">
                <label>Revenue (&euro;)</label>
                <input type="number" min="-1000000000000" max="1000000000000" step="1" name="revenue">
                <label>Net income (&euro;)</label>
                <input type="number" min="-1000000000000" max="1000000000000" step="1" name="netIncome">
                <label>Securities</label>
                <input type="number" min="0" max="1000000000000" step="1" name="securities">
                <label>Security price (&euro;)</label>
                <input type="number" step="0.01" name="securityPrice">
                <label>Dividends (&euro;)</label>
                <input type="number" step="0.01" name="dividends">
                <button name="save">Save</button>
            </div>
        </div>
    </template>
</popup-window>
`;

export default class CompanyEdit extends HTMLElement {

    static get observedAttributes() {
        return ['company-id'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case 'company-id':
                this.companyId = parseInt(newValue);
                break;
        }
    }

    connectedCallback() {
        this.render();
    }

    render() {
        let contentNode = document.importNode(template.content, true);
        this.appendChild(contentNode);
        this.querySelector('button[name=upload]').addEventListener('click', this.upload);
        this.querySelector('button[name=save]').addEventListener('click', this.save);
        this.bind();
    }

    bind() {
        let company = AppState.getCompany(this.companyId);
        this.querySelector('input[name=name]').value = company.name;
        this.querySelector('input[name=logo]').value = company.logo;
        this.querySelector('[data-key=logoImg]').src = company.logo;
        this.querySelector('input[name=established]').value = company.established;
        this.querySelector('input[name=employees]').value = company.employees;
        this.querySelector('input[name=revenue]').value = company.revenue;
        this.querySelector('input[name=netIncome]').value = company.netIncome;
        this.querySelector('input[name=securities]').value = company.securities;
        this.querySelector('input[name=securityPrice]').value = company.securityPrice;
        this.querySelector('input[name=dividends]').value = company.dividends;
    }

    openPopup = () => {
        let popup = this.querySelector('popup-window');
        popup.openPopup();
    }

    closePopup = () => {
        let popup = this.querySelector('popup-window');
        popup.closePopup();
    }

    upload = async () => {
        try {
            const logoFile = this.querySelector('input[name=file]').files[0];
            if (logoFile) {
                let uploadResponse = await FetchApi.postFile(logoFile);
                this.querySelector('[data-key=logoImg]').src = uploadResponse.url;
                this.querySelector('input[name=logo]').value = uploadResponse.url;
            }
        } catch (e) {
            Util.handleError(e, App.openLoginBox);
        }
    }

    save = async () => {
        try {
            let name = this.querySelector('input[name=name]').value;
            let logo = this.querySelector('input[name=logo]').value;
            let established = this.querySelector('input[name=established]').value;
            let employees = this.querySelector('input[name=employees]').value;
            let revenue = this.querySelector('input[name=revenue]').value;
            let netIncome = this.querySelector('input[name=netIncome]').value;
            let securities = this.querySelector('input[name=securities]').value;
            let securityPrice = this.querySelector('input[name=securityPrice]').value;
            let dividends = this.querySelector('input[name=dividends]').value;

            let company = {
                id: this.companyId ? this.companyId : 0,
                name: name,
                logo: logo,
                established: established,
                employees: !Util.isEmpty(employees) ? employees : null,
                revenue: !Util.isEmpty(revenue) ? revenue : null,
                netIncome: !Util.isEmpty(netIncome) ? netIncome : null,
                securities: !Util.isEmpty(securities) ? securities : null,
                securityPrice: !Util.isEmpty(securityPrice) ? securityPrice : null,
                dividends: !Util.isEmpty(dividends) ? dividends : null
            };

            let errors = new CompanyValidator(company)
                .validateName()
                .validateLogo()
                .validateEstablished()
                .validateEmployees()
                .validationErrors;

            if (errors.length > 0) {
                this.querySelector('error-box').errors = errors;
                this.querySelector('[data-key=companyEditFormContainer]').scrollTop = 0;
            } else {
                await FetchApi.postCompany(company);
                AppState.updateCompany(company);
                this.companySavedCallback();
            }
        } catch (e) {
            this.closePopup();
            Util.handleError(e, App.openLoginBox);
        }
    }

    companySavedCallback() {
        this.parentNode.render();
    }
}

window.customElements.define('company-edit', CompanyEdit);