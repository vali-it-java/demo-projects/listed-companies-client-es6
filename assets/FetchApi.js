import Config from './Config.js';
import Util from './Util.js';
import Auth from './Auth.js';

async function login(credentials) {
    let response = await fetch(
        `${Config.API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    );
    return await Util.handleJsonResponse(response);
}

async function fetchCompanies() {
    let response = await fetch(
        `${Config.API_URL}/companies`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${Auth.getToken()}`
            }
        }
    );
    return await Util.handleJsonResponse(response);
}

async function postFile(file) {
    let formData = new FormData();
    formData.append("file", file);

    let response = await fetch(
        `${Config.API_URL}/files/upload`,
        {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${Auth.getToken()}`
            },
            body: formData
        }
    );
    return await Util.handleJsonResponse(response);
}

async function postCompany(company) {
    let response = await fetch(
        `${Config.API_URL}/companies`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${Auth.getToken()}`
            },
            body: JSON.stringify(company)
        }
    );
    Util.handleResponse(response);
}

async function deleteCompany(id) {
    let response = await fetch(
        `${Config.API_URL}/companies/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${Auth.getToken()}`
            }
        }
    );
    Util.handleResponse(response);
}

export default {
    login,
    fetchCompanies,
    postFile,
    postCompany,
    deleteCompany
};