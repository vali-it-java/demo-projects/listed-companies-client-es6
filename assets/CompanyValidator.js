export default class CompanyValidator {

    constructor(company) {
        this.company = company;
        this.errors = [];
    }

    get validationErrors() {
        return this.errors;
    }

    validateName() {
        if (!this.company.name || this.company.name.length < 2) {
            this.errors.push('Incorrect company name.');
        }
        return this;
    }

    validateLogo() {
        if (!this.company.logo ||
            !this.company.logo.startsWith('http://') && !this.company.logo.startsWith('https://')
        ) {
            this.errors.push('Incorrect company logo URL.');
        }
        return this;
    }

    validateEstablished() {
        if (!this.company.established || new Date(this.company.established).getTime() > new Date().getTime()) {
            this.errors.push('Incorrect or missing company established date (must be in the past).');
        }
        return this;
    }

    validateEmployees() {
        if (!this.company.employees || this.company.employees < 1) {
            this.errors.push('Incorrect employees count (must be at least 1).');
        }
        return this;
    }
}