import Util from './Util.js';
import AppState from './AppState.js';
import FetchApi from './FetchApi.js';

window.addEventListener('load', async () => {
    load();
});

async function load(fetch = true) {
    try {
        if (fetch) {
            let companies = await FetchApi.fetchCompanies();
            AppState.setCompanies(companies);
        }
        document.querySelector('session-box').render();
        document.querySelector('company-chart').render();
        document.querySelector('company-list').render();
    } catch (e) {
        Util.handleError(e, openLoginBox);
    }
}

function openLoginBox() {
    document.querySelector('login-box').openPopup();
}

export default {
    load,
    openLoginBox
};